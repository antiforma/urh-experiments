---
layout: home
permalink: "/"
title: "Universal Rayhatcher Experiments"
description: ""
meta_description: "Universal Rayhatcher Experiments"
meta_title: Universal Rayhatcher Experiments
subscribe: false
published: true

notes:
  heading: ""
  sub_heading: ""
  limit: 6
  sort: date # date | weight
  view_more_button_text: ""
  view_more_button_link: ""
  view_more_button_align: ""
  columns: 1 # 1 | 2 | 3 | 4
---

This is a collection of experiments, SDF code and notes I'm keeping while studying and creating with [Universal Rayhatcher](https://www.fxhash.xyz/generative/slug/universal-rayhatcher).

- [Universal Rayhatcher minted works](https://fxwho.xyz/fxhash/urh) 
- [framework documentation](https://gist.github.com/tripzilch/9042195ae6f62901909ff7c99aeb8fcc)
- [development environment](https://extreme-rayhatching.netlify.app/) 
- [devevelopment environemnt docs](https://gist.github.com/tripzilch/54b48ccdc7c55c6b196c6599e6a40800) 

Notable links about Rayhatching and SDFs:

- [https://iquilezles.org/articles/distfunctions/](https://iquilezles.org/articles/distfunctions/) (large SDFs documentation)
- [https://www.youtube.com/watch?v=EGvuSOvuREQ](https://www.youtube.com/watch?v=EGvuSOvuREQ) (workshop going over how to implement raymarching)
